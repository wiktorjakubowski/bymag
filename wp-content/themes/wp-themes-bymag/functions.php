<?php

set_post_thumbnail_size( 200, 155, true );
add_image_size( 'szyja', 400, 400 );


// Load js and css files to index.php
function custom_scripts() {

    wp_enqueue_script(
        'jQuery',
        get_stylesheet_directory_uri() . '/bower_components/jquery/dist/jquery.min.js'
    );
//    wp_enqueue_script(
//        'jQuery-easing',
//        get_stylesheet_directory_uri() . '/bower_components/jquery.easing/js/jquery.easing.min.js'
//    );
//    wp_enqueue_script(
//        'jQuery-animate',
//        get_stylesheet_directory_uri() . '/js/superslides/jquery.animate-enhanced.min.js'
//    );


    wp_enqueue_script(
        'app',
        get_stylesheet_directory_uri() . '/js/app.js'
    );


//    wp_localize_script(
//        'app',
//        'customLocalized',
//        array(
//            'templates' => trailingslashit( get_template_directory_uri() ) . 'templates/'
//        )
//    );

    wp_enqueue_style(
        'theme-style',
        get_template_directory_uri() . '/style.css',
        false,
        '0.1.0',
        'all'
    ); // Inside a parent theme

}

// This theme uses wp_nav_menu() in two locations.
function register_my_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'extra-menu' => __( 'Extra Menu' )
        )
    );
}

/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
add_theme_support( 'title-tag' );


add_action( 'init', 'register_my_menus' );
add_theme_support( 'menus' );
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

    register_sidebar( array(
        'name'          => 'Home right sidebar',
        'id'            => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );
?>
