<?php get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php the_content('czytaj dalej'); ?>
    <?php endwhile;
    else: ?>
        <p><?php _e('Przepraszam, ale nie ma żadnych postów spełniających podane kryteria.'); ?></p>
    <?php endif; ?>

<?php get_footer(); ?>