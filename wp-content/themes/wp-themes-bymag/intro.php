﻿<?php
/*
Template Name: intro
*/
?>

<!DOCTYPE html>
<head>
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body>

<div id="intro">
    <a href="/home" id="logo"><img src="<?php echo get_template_directory_uri() ?>/img/logo_05.png" alt="BYMAG"/></a> <!-- end logo -->
</div>
<?php wp_footer(); ?>
</body>
</html>
