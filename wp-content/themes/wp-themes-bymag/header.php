<!DOCTYPE html>
<head>
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="wrapper" class="container">
    <header id="header" class="row">
        <div class="col-sm-3">
            <a href="<?php bloginfo('home'); ?>" id="logo"><img src="<?php echo get_template_directory_uri() ?>/img/logo_05.png" alt="BYMAG"/></a>
        </div>
        <div class="col-sm-9">
            <nav id="navBar" class="pull-right">
                <?php wp_nav_menu(array('theme_location' => 'header-menu', 'depth' => 1)); ?>
            </nav>
        </div>
    </header>
    <!-- end top -->
    <main id="mainContent" class="row" role="main">